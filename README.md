# <div align="center"><img src="assets/logo/sttb.png" width="300" height="300">

[![Build Status - Cirrus][]][Build status]
[![Gitter Channel][]][Gitter badge]
[![Twitter handle][]][Twitter badge]
</div>

## Aplikasi Buku Kas

Aplikasi Buku Kas adalah aplikasi keuangan untuk Monitor transaksi keuangan, pemasukan dan pengeluaran anda. Membantu mengatur keuangan anda jadi lebih baik dengan aplikasi pembukuan keuangan gratis dari kami :)

## Anggota Kelompok

* [Destriyana Sarizqie (18111193)](https://gitlab.com/DestriyanaS)
* [Huda Nasnur Tajali (18111199)](https://gitlab.com/hudanasgor) - Ketua Kelompok
* [Ivan Syah (18111201)](https://gitlab.com/ivansyah420)
* [Marisa (18111209)](https://gitlab.com/marisa998)
* [Yusuf Abdul Rozzaq (18111235)](https://gitlab.com/usuv.official)

## Job Description Anggota

* [Destriyana Sarizqie](https://gitlab.com/DestriyanaS) : Membuat tampilan pengeluaran.
* [Huda Nasnur Tajali](https://gitlab.com/hudanasgor) : Sebagai Ketua dari Kelompok dan menentukan judul juga alur aplikasi.
* [Ivan Syah](https://gitlab.com/ivansyah420) : Membuat tampilan pemasukan.
* [Marisa](https://gitlab.com/marisa998) : Membuat tampilan profile.
* [Yusuf Abdul Rozzaq](https://gitlab.com/usuv.official) : Membuat template design dart, finishing aplikasi.

## Documentation

* Tampilan Login
    <br><img src="assets/ss/login.jpg"  width="340" height="640">

* Tampilan Home
    <br><img src="assets/ss/home.jpg"  width="340" height="640">

* Tampilan Menu
    <br><img src="assets/ss/menu.jpg"  width="340" height="640">

* Tampilan Pemasukan
    <br><img src="assets/ss/pemasukan.jpg"  width="340" height="640">

* Tampilan Pengeluaran
    <br><img src="assets/ss/pengeluaran.jpg"  width="340" height="640">

* Tampilan Profil
    <br><img src="assets/ss/biodata.jpg"  width="340" height="640">

## Terima Kasih

[Flutter logo]: https://raw.githubusercontent.com/flutter/website/master/src/_assets/image/flutter-lockup.png
[flutter.dev]: https://flutter.dev
[Build Status - Cirrus]: https://api.cirrus-ci.com/github/flutter/flutter.svg
[Build status]: https://cirrus-ci.com/github/flutter/flutter/master
[Gitter Channel]: https://badges.gitter.im/flutter/flutter.svg
[Gitter badge]: https://gitter.im/flutter/flutter?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge
[Twitter handle]: https://img.shields.io/twitter/follow/flutterdev.svg?style=social&label=Follow
[Twitter badge]: https://twitter.com/intent/follow?screen_name=usuv_
[FFI]: https://flutter.dev/docs/development/platform-integration/c-interop
[platform channels]: https://flutter.dev/docs/development/platform-integration/platform-channels
[interop example]: https://github.com/flutter/flutter/tree/master/examples/platform_channel