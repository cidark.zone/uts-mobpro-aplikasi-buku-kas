import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:uts_mobpro_aplikasi_buku_kas/profile.dart';
import 'package:uts_mobpro_aplikasi_buku_kas/pemasukan.dart';
import 'package:uts_mobpro_aplikasi_buku_kas/pengeluaran.dart';

class MainScreen extends StatefulWidget {
  final String nama;
  MainScreen({Key key, @required this.nama}) : super(key: key);

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  String namee;

  @override
  void initState() {
    super.initState();
    namee = widget.nama;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: Text("Aplikasi Buku Kas"),
        actions: <Widget>[
          PopupMenuButton(
            itemBuilder: (context) {
              return [
                PopupMenuItem(
                  child: Text("Profile"),
                  value: 1,
                )
              ];
            },
            onSelected: (value) {
              if (value == 1) {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ProfileScreen(nama: namee)));
              }
            },
          )
        ],
      ),
      floatingActionButton: customFlatActionButton(context),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 20.0),
        children: <Widget>[
          SizedBox(
            height: 20.0,
          ),
          Text(
            "Pilih Fitur :",
            textAlign: TextAlign.center,
            style: TextStyle(
                color: Theme.of(context).primaryColor,
                fontSize: 20.0,
                fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: 20.0,
          ),
          SizedBox(
            height: 50.0,
            child: RawMaterialButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(50.0)),
              elevation: 10.0,
              fillColor: Theme.of(context).accentColor,
              child: Center(child: Text("Tambah Pemasukan")),
              onPressed: () async {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => PemasukanScreen(nama: namee)));
              },
            ),
          ),
          SizedBox(
            height: 20.0,
          ),
          SizedBox(
            height: 50.0,
            child: RawMaterialButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(50.0)),
              elevation: 10.0,
              fillColor: Theme.of(context).accentColor,
              child: Center(child: Text("Tambah Pengeluaran")),
              onPressed: () async {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => PengeluaranScreen(nama: namee)));
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget customFlatActionButton(BuildContext context) {
    return Hero(
      tag: "Profile",
      child: SizedBox(
        height: 40.0,
        width: 90.0,
        child: RawMaterialButton(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(50.0)),
          elevation: 10.0,
          fillColor: Theme.of(context).accentColor,
          child: Center(child: Text("Profile")),
          onPressed: () async {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => ProfileScreen(nama: namee)));
          },
        ),
      ),
    );
  }
}
