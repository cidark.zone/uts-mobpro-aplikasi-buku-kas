import 'package:flutter/material.dart';
import 'package:uts_mobpro_aplikasi_buku_kas/component/Widget.dart';
import 'package:uts_mobpro_aplikasi_buku_kas/component/Validation.dart';
import 'package:uts_mobpro_aplikasi_buku_kas/home.dart';

void main() {
  return runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    theme: ThemeData(
        primaryColor: Color(0xff3f3d56),
        accentColor: Color(0xffffc107),
        textTheme: TextTheme(button: TextStyle(color: Color(0xff3f3d56))),
        buttonColor: Color(0xffffc107),
        scaffoldBackgroundColor: Colors.white),
    home: MyApp(),
  ));
}

class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  @override
  _MyAppScreenState createState() => _MyAppScreenState();
}

class _MyAppScreenState extends State<MyApp> with Validation {
  TextEditingController _etName = TextEditingController();
  bool _validate = false;

  final FocusNode _focusName = FocusNode();
  final _formKey = GlobalKey<FormState>();
  String nama = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: Text("Aplikasi Buku Kas"),
      ),
      floatingActionButton: customFlatActionButton(context),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      body: Form(
        key: _formKey,
        child: ListView(
          physics: BouncingScrollPhysics(),
          children: <Widget>[
            SizedBox(
              height: 30.0,
            ),
            Text(
              "Buku Kas",
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: Theme.of(context).primaryColor,
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold),
            ),
            SizedBox(
              height: 200.0,
              width: MediaQuery.of(context).size.width,
              child: Container(
                padding: EdgeInsets.all(30.0),
                height: 80.0,
                width: 80.0,
                child: Image.asset(
                  "assets/images/dreamer.webp",
                  width: 200.0,
                ),
              ),
            ),
            Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 20.0, vertical: 1.0),
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 10.0,
                  ),
                  CustomTextField(
                    label: "Nama",
                    focusNode: _focusName,
                    controller: _etName,
                    maxLines: 1,
                    textInputAction: TextInputAction.newline,
                    onSubmitted: (val) {
                      nama = val;
                    },
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget customFlatActionButton(BuildContext context) {
    return Hero(
      tag: "primarybutton",
      child: SizedBox(
        width: MediaQuery.of(context).size.width - 20,
        child: RawMaterialButton(
          padding: EdgeInsets.symmetric(vertical: 20.0),
          child: Text(
            "Simpan",
            style: TextStyle(
                color: Theme.of(context).textTheme.button.color,
                fontSize: 18.0),
          ),
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
          elevation: 5.0,
          fillColor: Theme.of(context).accentColor,
          onPressed: () async {
            //Isi Disini saat tombol simpan
            Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => MainScreen(
                    nama: _etName.text.trim(),
                  ),
                ));
          },
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    _focusName.dispose();
  }
}
