import 'package:flutter/cupertino.dart';
import 'package:uts_mobpro_aplikasi_buku_kas/component/Widget.dart';
import 'package:flutter/material.dart';
import 'package:uts_mobpro_aplikasi_buku_kas/home.dart';
import 'function.dart';

class PengeluaranScreen extends StatefulWidget {
  final String nama;
  PengeluaranScreen({Key key, @required this.nama}) : super(key: key);

  @override
  _PengeluaranScreenState createState() => _PengeluaranScreenState();
}

class _PengeluaranScreenState extends State<PengeluaranScreen> {
  TextEditingController _etPengeluaran = TextEditingController();
  TextEditingController _etDesc = TextEditingController();
  bool _validate = false;

  final FocusNode _focusPengeluaran = FocusNode();
  final FocusNode _focusDesc = FocusNode();
  final _formKey = GlobalKey<FormState>();
  String namee;

  @override
  void initState() {
    super.initState();
    namee = widget.nama;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: Text("Aplikasi Buku Kas"),
        actions: <Widget>[
          PopupMenuButton(
            itemBuilder: (context) {
              return [
                PopupMenuItem(
                  child: Text("Home"),
                  value: 1,
                )
              ];
            },
            onSelected: (value) {
              if (value == 1) {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => MainScreen(nama: namee)));
              }
            },
          )
        ],
      ),
      floatingActionButton: customFlatActionButton(context),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      body: Form(
        key: _formKey,
        child: ListView(
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          children: <Widget>[
            Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 20.0, vertical: 1.0),
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 30.0,
                  ),
                  Text(
                    "Pengeluaran Kas",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                        color: Theme.of(context).primaryColor,
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: 30.0,
                  ),
                  CustomTextField(
                    label: "Nominal",
                    textInputType: TextInputType.number,
                    focusNode: _focusPengeluaran,
                    controller: _etPengeluaran,
                    maxLines: 1,
                    textInputAction: TextInputAction.next,
                    onSubmitted: (val) {
                      MyFunction.focusSwitcher(
                          context, _focusPengeluaran, _focusDesc);
                    },
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  CustomTextField(
                    label: "Deskripsi",
                    focusNode: _focusDesc,
                    controller: _etDesc,
                    maxLines: 1,
                    textInputAction: TextInputAction.newline,
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget customFlatActionButton(BuildContext context) {
    return Hero(
      tag: "primarybutton",
      child: SizedBox(
        width: MediaQuery.of(context).size.width - 20,
        child: RawMaterialButton(
          padding: EdgeInsets.symmetric(vertical: 20.0),
          child: Text(
            "Simpan",
            style: TextStyle(
                color: Theme.of(context).textTheme.button.color,
                fontSize: 18.0),
          ),
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
          elevation: 5.0,
          fillColor: Theme.of(context).accentColor,
          onPressed: () async {
            //Isi Disini saat tombol simpan
          },
        ),
      ),
    );
  }
}
