import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:uts_mobpro_aplikasi_buku_kas/home.dart';

class ProfileScreen extends StatefulWidget {
  final String nama;
  ProfileScreen({Key key, @required this.nama}) : super(key: key);

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  String namee;

  @override
  void initState() {
    super.initState();
    namee = widget.nama;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: Text("Aplikasi Buku Kas"),
        actions: <Widget>[
          PopupMenuButton(
            itemBuilder: (context) {
              return [
                PopupMenuItem(
                  child: Text("Home"),
                  value: 1,
                )
              ];
            },
            onSelected: (value) {
              if (value == 1) {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => MainScreen(nama: namee)));
              }
            },
          )
        ],
      ),
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 20.0),
        children: <Widget>[
          SizedBox(
            height: 30.0,
          ),
          Text(
            "Profile",
              textAlign: TextAlign.center,
            style: TextStyle(
                color: Theme.of(context).primaryColor,
                fontSize: 20.0,
                fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: 30.0,
          ),
          Image.asset(
            "assets/images/logo.webp",
            height: 200.0,
          ),
          SizedBox(
            height: 30.0,
          ),
          Text(
            "Nama",
            textAlign: TextAlign.left,
            style: TextStyle(
                color: Theme.of(context).primaryColor,
                fontSize: 15.0,
                fontWeight: FontWeight.bold),
          ),
          Text(
            widget.nama,
            textAlign: TextAlign.left,
            style: TextStyle(
                color: Theme.of(context).primaryColor,
                fontSize: 17.0,
                fontWeight: FontWeight.normal),
          ),
          SizedBox(
            height: 15.0,
          ),
          Text(
            "Hobi",
            textAlign: TextAlign.left,
            style: TextStyle(
                color: Theme.of(context).primaryColor,
                fontSize: 15.0,
                fontWeight: FontWeight.bold),
          ),
          Text(
            (widget.nama == "Yusuf") ? "Code Programming" : ((widget.nama == "Huda") ? "Sepak Bola" : ((widget.nama == "Ivan Syah") ? "Travelling" : ((widget.nama == "Destriyana") ? "Listening Music" : ((widget.nama == "Marisa") ? "Menggambar" : "Tidak Tau")))),
            textAlign: TextAlign.left,
            style: TextStyle(
                color: Theme.of(context).primaryColor,
                fontSize: 17.0,
                fontWeight: FontWeight.normal),
          ),
          SizedBox(
            height: 15.0,
          ),
          Text(
            "Tempat Tanggal Lahir",
            textAlign: TextAlign.left,
            style: TextStyle(
                color: Theme.of(context).primaryColor,
                fontSize: 15.0,
                fontWeight: FontWeight.bold),
          ),
          Text(
            (widget.nama == "Yusuf") ? "Bandung, 18 Oktober 2000" : ((widget.nama == "Huda") ? "Bandung, 26 Agustus 1998" : ((widget.nama == "Ivan Syah") ? "Bandung, 07 febuari 1996" : ((widget.nama == "Destriyana") ? "Kurang Tau" : ((widget.nama == "Marisa") ? "Bandung, 01 Maret 1998" : "Tidak Tau")))),
            textAlign: TextAlign.left,
            style: TextStyle(
                color: Theme.of(context).primaryColor,
                fontSize: 17.0,
                fontWeight: FontWeight.normal),
          ),
          SizedBox(
            height: 15.0,
          ),
          Text(
            "Alamat",
            textAlign: TextAlign.left,
            style: TextStyle(
                color: Theme.of(context).primaryColor,
                fontSize: 15.0,
                fontWeight: FontWeight.bold),
          ),
          Text(
            (widget.nama == "Yusuf") ? "Jalan Rajawali Timur" : ((widget.nama == "Huda") ? "Curug Candung" : ((widget.nama == "Ivan Syah") ? "Kopo Permai" : ((widget.nama == "Destriyana") ? "Kopo" : ((widget.nama == "Marisa") ? "Caringin" : "Tidak Tau")))),
            textAlign: TextAlign.left,
            style: TextStyle(
                color: Theme.of(context).primaryColor,
                fontSize: 17.0,
                fontWeight: FontWeight.normal),
          ),
          SizedBox(
            height: 15.0,
          )
        ],
      ),
    );
  }
}
